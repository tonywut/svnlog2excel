svnlog2excel.jar是用于将xml格式的svnlog转成excel表格。使用方法为
java -jar svnlog2excel.jar changelog.xml changelog.xls

svnlog2excel.sh 将指定SVN路径的版本记录导出为xml格式，去掉第一个版本（一般为初始化代码且数量太大无需处理）,并且调用svnlog2excel.jar 生成excel文件。

jxl.jar为读写excel要调用的包