#!/bin/bash
if [ ! $# = 1 ]  
then
	echo "usage: $0 svn://192.168.0.84/ALPS.JB.MP.V1.6_SIGNAL77_CU_JB_GPL/trunk";
	exit 0
fi
svnpath=${1};
REV=$(svn info ${svnpath} | grep 最后修改的版本  | cut -d " " -f 2) ;
svn log ${svnpath} -v --xml -r ${REV}:2 > changelog.xml  ;
filename=${1#svn://192.168.0.84/};
java -jar svnlog2excel.jar changelog.xml ${filename//\//_}.xls;
rm changelog.xml
