package com.tony;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class svnlogXMLHandler  extends DefaultHandler {
	private List<SvnLog> svnlogs;

	private SvnLog svnlog;	
    private String whichElement;  
    private String paths = "";
    private int pathNum = 0;
     
	public final static String LOGENTRY  = "logentry";
	public final static String REVISION  = "revision";
	public final static String AUTHOR  = "author";
	public final static String DATE  = "date";
	public final static String PATHS  = "paths";
	public final static String PATH  = "path";
	public final static String ACTION  = "action";	
	public final static String MSG  = "msg";	
	
	public List<SvnLog> getSvnLogs(){
		return svnlogs;
	}
	
	@Override  
	public void startDocument() throws SAXException { 
		svnlogs = new ArrayList<SvnLog>();
	}
	@Override  
	public void characters(char[] ch, int start, int length) {
		if(whichElement!=null){
			String valueString = new String(ch, start, length);
			if(AUTHOR.equals(whichElement)){
				svnlog.setAuthor(valueString);
			}else if(DATE.equals(whichElement)){
				svnlog.setDate(valueString);
			}else if(PATH.equals(whichElement)){
				paths = paths + valueString ;
			}else if(MSG.equals(whichElement)){
				svnlog.setMsg(valueString);
			}
		}
	}
	@Override  
	public void startElement (String uri, String localName, String name,  
				Attributes attributes) throws SAXException { 
		if(LOGENTRY.equals(name)) {
			svnlog = new SvnLog();
			svnlog.setRevision(new String(attributes.getValue(REVISION)));
		}else if(PATH.equals(name)) {
	        paths = paths + (pathNum != 0? System.getProperty("line.separator"):"") + (new String(attributes.getValue(ACTION))) + " ";
			pathNum ++ ;
		}else if(PATHS.equals(name)){
			pathNum = 0;
		}
		whichElement = name;
	}

	@Override 
	public void endElement(String uri, String localName, String name)
			throws SAXException {
		if(LOGENTRY.equals(name) ) {
			svnlogs.add(svnlog);
			svnlog = null;
		} else if (PATHS.equals(name)) {
			svnlog.setPaths(paths);
			paths = "";
		}
		whichElement = null;
	}
	@Override 
	public void endDocument () {
	}
}
