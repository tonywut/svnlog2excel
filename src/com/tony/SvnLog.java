package com.tony;

public class SvnLog {
	private String revision = "";
	private String author = "";
	private String date = "";
	private String paths = "";
	private String msg = "";
	
	public SvnLog(){
	}
	
	public void setRevision(String s){
		this.revision = s;
	}
	public String getRevision(){
		return this.revision;
	}

	public void setAuthor(String s){
		this.author = s;
	}
	public String getAuthor(){
		return this.author;
	}
	public void setDate(String s){
		this.date = s;
	}
	public String getDate(){
		return this.date;
	}
	public void setPaths(String s){
		this.paths = s;
	}
	public String getPaths(){
		return this.paths;
	}
	public void setMsg(String s){
		this.msg = s;
	}
	public String getMsg(){
		return this.msg;
	}
}
