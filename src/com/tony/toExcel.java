package com.tony;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;


public class toExcel {
	public static List<SvnLog> getSvnLogs(String filename) throws Exception {
		InputStream inStream = new FileInputStream(new File(filename));
        SAXParserFactory spf = SAXParserFactory.newInstance(); // 初始化sax解析器  
        SAXParser sp = spf.newSAXParser(); // 创建sax解析器  
        svnlogXMLHandler handler = new svnlogXMLHandler();  
        sp.parse(inStream, handler);  
        return handler.getSvnLogs(); 
	}
    /**
     * @param args
     */
    public static void main(String[] args) {
    	if(args.length != 2) {
    		System.out.println("Please input src file(***.xml) and target file(***.xls)");
    		System.out.println("ex: java -jar svnlog2excel.jar changlog.xml test2.xls");
    		return;
    	}
    	String targetFile = args[1];
    	String srcFile = args[0];
        File file = new File(targetFile);
    	Label label;
        try {
        	List<SvnLog> svnlogs = getSvnLogs(srcFile);
            WritableWorkbook workbook = Workbook.createWorkbook(file);
            WritableSheet sheet = workbook.createSheet("test1", 0);

            WritableFont wtf = new WritableFont(WritableFont.createFont("宋体"),14,WritableFont.BOLD,false);
            WritableCellFormat wcfmt = new WritableCellFormat(wtf);
            wcfmt.setAlignment(jxl.format.Alignment.CENTRE);
            
            int c= 0;
            sheet.setColumnView(c, 14);
        	label = new Label(c++, 0, svnlogXMLHandler.REVISION, wcfmt);
            sheet.addCell(label);
            sheet.setColumnView(c, 14);
        	label = new Label(c++, 0, svnlogXMLHandler.AUTHOR, wcfmt);
            sheet.addCell(label);
            sheet.setColumnView(c, 11);
        	label = new Label(c++, 0, svnlogXMLHandler.DATE, wcfmt);
            sheet.addCell(label);
            sheet.setColumnView(c, 40);
        	label = new Label(c++, 0, svnlogXMLHandler.MSG, wcfmt);
            sheet.addCell(label);
            sheet.setColumnView(c, 100);
        	label = new Label(c++, 0, svnlogXMLHandler.PATHS, wcfmt);
            sheet.addCell(label);
            
            WritableFont wtf2 = new WritableFont(WritableFont.createFont("Arial"),10,WritableFont.NO_BOLD,false);
            WritableCellFormat wcfmt2 = new WritableCellFormat(wtf2);
            wcfmt2.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
            
            WritableCellFormat wcfmt3 = new WritableCellFormat(wtf2);
            wcfmt3.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
            wcfmt3.setAlignment(jxl.format.Alignment.CENTRE);
            
            for (int i=0; i<svnlogs.size(); i++){
            	int j = 0;
            	label = new Label(j++, i+1, svnlogs.get(i).getRevision(), wcfmt3);
                sheet.addCell(label);
            	label = new Label(j++, i+1, svnlogs.get(i).getAuthor(), wcfmt2);
                sheet.addCell(label);
            	label = new Label(j++, i+1, svnlogs.get(i).getDate(), wcfmt2);
                sheet.addCell(label);
            	label = new Label(j++, i+1, svnlogs.get(i).getMsg(), wcfmt2);
                sheet.addCell(label);
            	label = new Label(j++, i+1, svnlogs.get(i).getPaths(), wcfmt2);
                sheet.addCell(label);
            }
            workbook.write();
            workbook.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (RowsExceededException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
